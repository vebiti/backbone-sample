<?php

/*
 * v. 2012-11-30 bei newsletterconfirm dynamischer Param "userip";
 * v. 2013-01-09 Cookie wird nicht mehr übergeben (wegen Logout-Bug im Adminbereich vom eHotelier)
 * v. 2013-01-10 Header wird nicht mehr geändert
 * v. 2013-02-25 Caching & getversion
 * v. 2013-03-12 useragent
 * v. 2013-03-28 Param "userip" wird jetzt immer übergeben.
 * v. 2013-03-29 Works with PHP Version < 5.2.1.
 * v. 2013-04-02 Als Wert für clearcache kann jetzt alles übergeben werden, nicht nur clearcache=1.
 * v. 2013-04-05 open_basedir restriction in effect beim caching: wenn Fehler dann wird nicht mehr versucht zu cachen.
 * v. 2013-05-14 search & validate referrer data from google-cookie or client-cookies
 * v. 2013-05-15 promotion, promotionvendor & sourceofbusiness werden jetzt automatisch eingefügt falls diese leer sind.
 * v. 2013-05-16 Bug in Parameterübergabe ("[" und "]")
 * v. 2013-05-17 Automatische Updatefunktion, bnproxy.php aufgeteilt in bnproxy.php und bnproxyfunctions.php
 * v. 2014-01-08 Bug mit zwei '//' bei $thedir
 */

$version = '2014-01-08';

if (!function_exists('sys_get_temp_dir')) {

    function sys_get_temp_dir() {
        if (!empty($_ENV['TMP'])) {
            return realpath($_ENV['TMP']);
        }
        if (!empty($_ENV['TMPDIR'])) {
            return realpath($_ENV['TMPDIR']);
        }
        if (!empty($_ENV['TEMP'])) {
            return realpath($_ENV['TEMP']);
        }
        $tempfile = tempnam(__FILE__, '');
        if (file_exists($tempfile)) {
            unlink($tempfile);
            return realpath(dirname($tempfile));
        }
        return null;
    }

}
$test = isset($_GET['test']) && $_GET['test'] == 'true' ? true : false;
$server = $test ? 'http://192.168.2.145' : 'http://admin.ehotelier.it';


$thedir = sys_get_temp_dir();
if (substr($thedir,-1)!=DIRECTORY_SEPARATOR){
    $thedir.=DIRECTORY_SEPARATOR;
}


$bnproxyfunctionsfilename = $thedir . "bnproxyfunctions_cache_" . $_SERVER['HTTP_HOST'];

if (isset($_GET['updatebnproxy']) || !is_file($bnproxyfunctionsfilename) || !is_readable($bnproxyfunctionsfilename)){
    $bnproxyfunctions=file_get_contents($server.'/bnproxyfunctions.php.data');
    if (strpos($bnproxyfunctions,'$bnproxyfunctionsversion')!==false){

        if ($bnproxyfunctions) file_put_contents($bnproxyfunctionsfilename,$bnproxyfunctions);

    }
}

if (isset($_GET['getversion'])) {
    echo 'Version: '.$version;
}
if (is_file($bnproxyfunctionsfilename) && is_readable($bnproxyfunctionsfilename)){
    require($bnproxyfunctionsfilename);
}
if (!isset($bnproxyfunctionsversion)) die('ERROR: could not load bnproxyfunctions!');



?>
