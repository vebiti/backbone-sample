/*jslint white: true, browser: true, devel: true, nomen: true*/

(function () {

    'use strict';

    var Backbone = require('backbone'),
        _ = require('lodash'),
        $ = require('jquery');

    window.$ = $;
    window.jQuery = $;

    var parsley = require('parsley');

    Backbone.$ = $;

    require('./libs/request.js');
    require('./jquery-ui/jquery.ui.core.js');
    require('./jquery-ui/jquery.ui.datepicker.js');

    var app = require('./app.js');

    app.Models.Accomodation = require('./models/accomodation.js');
    app.Collections.Accomodations = require('./collections/accomodations.js');
    app.Views.Filters = require('./views/filters.js');
    app.Views.Accomodation = require('./views/accomodation.js');
    app.Views.Accomodations = require('./views/accomodations.js');
    app.Views.InquiryForm = require('./views/inquiry-form.js');


    app.templates.accomodation = require('./templates/accomodation.hbs');


    // Create instances of Collections and Views
    var accomodationsCollection = new app.Collections.Accomodations();
    accomodationsCollection.fetch();

    new app.Views.Filters({
        collection: accomodationsCollection
    });

    new app.Views.Accomodations({
        collection: accomodationsCollection
    });

    new app.Views.InquiryForm();

}());
