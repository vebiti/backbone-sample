/*global Backbone, _*/

(function () {

    'use strict';

    var Backbone = require('backbone'),
        _ = require('lodash'),
        $ = require('jquery');

    module.exports = {
        Models: {},
        Collections: {},
        Views: {},
        templates: {},

        vent: _.extend({}, Backbone.Events)
    };

}());
