/*global Backbone, _*/

var app = app || {
    Models: {},
    Collections: {},
    Views: {},
    templates: {},

    vent: _.extend({}, Backbone.Events)
};
