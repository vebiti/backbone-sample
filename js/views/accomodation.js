/*global Backbone*/

(function () {
    'use strict';

    var app = require('../app.js'),
        Backbone = require('backbone');

    module.exports = Backbone.View.extend({
        tagName: 'li',
        className: 'clearfix',
        events: {
            'click button': 'inquiry'
        },

        inquiry: function () {
            app.vent.trigger('inquiry', this.model.get('id'));
        },

        render: function () {
            //console.log(this.model.toJSON());

            var output = app.templates.accomodation({content: this.model.toJSON()});
            this.$el.append(output);
            return this;
        }

    });
}());
