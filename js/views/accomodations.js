/*global document, Backbone, _*/

(function () {
    'use strict';

    var app = require('../app.js'),
        Backbone = require('backbone'),
        _ = require('lodash');

    module.exports = Backbone.View.extend({
        el: '#accView',

        docFrag: document.createDocumentFragment(),

        initialize: function () {
            app.vent.on('filterStateChange', this.render, this);
            this.listenTo(this.collection, 'sync', _.bind(this.render, this, undefined));
        },

        renderItem: function (model) {
            var itemView = new app.Views.Accomodation({model: model});
            this.docFrag.appendChild(itemView.render().el);
        },

        render: function (activeFilters) {

            var accomodationsFiltered = this.collection.customFilter(activeFilters);
            accomodationsFiltered.forEach(this.renderItem, this);

            this.$el.empty();
            this.$el.append(this.docFrag);
        }
    });

}());
