/*global Backbone, BN*/

(function () {
    'use strict';

    var app = require('../app.js'),
        Backbone = require('backbone'),
        $ = require('jquery');
        //BN = require('../libs/request.js');

    module.exports = Backbone.View.extend({
        initialize: function () {

            app.vent.on('inquiry', this.render);

        },

        render: function (accomodationId) {



            console.log(accomodationId);

            // always send request to the testhotel with id 1
            if (accomodationId !== 1) {
                accomodationId = 1;
            }

            new BN.Request({
                hotel_id: accomodationId,
                formcontainer_id: 'inquiryForm',
                language: 'de',
                url_success: '#',
                url_privacy: 'http://www.google.de',
                url_doubleopt: 'http://www.google.it',
                validation: true,
                test: false,
                room_filter: {
                    roomtypecategories: 'DZ'
                },
                fields: {
                    sourceofbusiness: 'test',
                    comment: '',
                    firstname: 'Otto',
                    otatransferrooms_0_asacode: 'DZ'
                },
                facebook: true,
                onLoad: function (form) {
                    // Nach Laden des Formulars
                    form.setField('todate','12.01.2014');
                },
                onSubmit: function () {
                    // Nach Abschicken des Formulars
                },
                onError: function () {
                    // Sollte eHotelier nach Submit einen Fehler zurückgeben
                },
                onSuccess: function () {
                    // Bevor auf die Dankeseite weitergeleitet wird
                }
            });

        }

    });
}());
