/*global Backbone, $*/

(function () {
    'use strict';

    var app = require('../app.js'),
        Backbone = require('backbone'),
        $ = require('jquery');

    module.exports = Backbone.View.extend({
        el: '#filters',
        events: {
            'change input[type="checkbox"]': 'setActiveFilters'
        },
        setActiveFilters: function () {
            var activeFilters = [];
            this.$el.find('input[type="checkbox"]:checked').each(function () {

                var value = parseInt($(this).val(), 10);
                activeFilters.push(value);
            });

            app.vent.trigger('filterStateChange', activeFilters);
        }
    });


}());
