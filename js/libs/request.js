(function($, window, document, undefined) {
    "use strict";

    // Alle Brandnamic JS haben einen globalen BN Namespace
    var BN = window.BN = window.BN || {};

    // AMD Modul
    if (typeof window.define === "function" && window.define.amd) {
        window.define("BN.Request", [], function() {
            return window.BN.Request;
        });
    }

    // Console Log IE
    if (typeof window.console === "undefined") {
        window.console = {log: function() {}, error: function() {}};
    }

    // Defaultwerte
    var defaults = {
        'formcontainer_id': undefined,
        'hotel_id': 1,
        'language': 'de',
        'validation': true,
        'test': false,
        'room_filter': undefined,
        'url_success': undefined,
        'url_privacy': undefined,
        'url_doubleopt': '',
        'facebook': false
    };

    // Konstruktor
    BN.Request = function(options) {
        // Defaults und Custom Configs werden gemerget
        this.options = $.extend({}, defaults, options);

        // Shorthand
        this.id = this.options.formcontainer_id;

        // Gecachtes Element verfügbar als DOM Element und $ Element
        this.el = $('#'+this.id)[0];
        this.$el = $('#'+this.id);

        this.hotelData = {};
        this.cache = {};


        // Ínitialisiere Events
        // Alt Date ein - und ausblenden
        this.$el.on('click', '.addAltDate', $.proxy(this.toggleAltDate, this));
        this.$el.on('click', '.removeAltDate', $.proxy(this.toggleAltDate, this));
        // Zimmer hinzufügen
        this.$el.on('click', '.addRoom', $.proxy(this.appendRoom, this));
        //Abschicken
        this.$el.on('click', '.submit button', $.proxy(this.submitForm, this));

        // Starte Modul
        this.init();
    };

    BN.Request.prototype = {

        init: function() {
            // Initialisiert Form nur, wenn el existiert
            if(this.el) {
                if(this.options.url_success) {
                    this.appendForm();
                } else {
                    console.error('url_success parameter nicht gesetzt');
                }
            }
        },

        // Füge Form ins DOM
        appendForm: function() {
            var promise1 = this.getFormFromServer(),
                promise2 = this.getRoomDataFromServer();

            $.when(promise1, promise2).done($.proxy(function(formData, hotelData) {
                // Speichert Zimmerdaten
                this.hotelData = hotelData[0].records[0].roomtypes;
                var form = $(formData[0].content);

                // Setzt privacy link
                if(this.options.url_privacy) {
                    var a = $('<a/>').attr({
                        'href': this.options.url_privacy,
                        'target': '_blank'
                    });

                    form.find('.bn--request__privacy label').wrapInner(a);
                }

                // Fügt Form DOM hinzu
                form.appendTo(this.$el.empty());

                this.showFb();

                this.initValidation();

                this.initUiDatepicker('#'+this.id+'_fromdate', '#'+this.id+'_todate');
                this.initUiDatepicker('#'+this.id+'_alternativefromdate', '#'+this.id+'_alternativetodate');

                this.appendRoom();

                // Eventhandler für Entwickler
                if(typeof this.options.onLoad !== 'undefined') {
                    this.options.onLoad(this);
                }

                // Setzt custom Felder
                this.setField(this.options.fields);
            }, this));
        },

        // Hole Form von eHotelier Server
        getFormFromServer: function() {
            return $.ajax({
                url: 'bnproxy.php?path='+this.options.language+'/getform/requestdavid&hotel_id='+this.options.hotel_id+'&formcontainer_id='+this.options.formcontainer_id+'&test='+this.options.test+(this.options.test ? '&clearcache=1' :''),
                dataType: 'json',
                success: function(data) {
                    if(!data.content) {
                        console.error('eHotelier Formular nicht gefunden');
                    }
                }
            });
        },

        // Hole Zimmer von eHotelier Server
        getRoomDataFromServer: function() {
            return $.ajax({
                url: 'bnproxy.php?path=services/hoteldata/'+this.options.language+'&portal_id=0&hotel_ids='+this.options.hotel_id+'&fields={"roomtypes":{"id":null,"minpersons":null,"maxpersons":null,"defaultpersons":null,"asacode":null,"name":null,"roomtypeboards":{"*":null}}}&test='+this.options.test+(this.options.test ? '&clearcache=1' :''),
                dataType: 'json',
                data: this.options.room_filter,
                success: function(data) {
                    if(!data.records.length) {
                        console.error('eHotelier Zimmerdaten nicht gefunden');
                    }
                }
            });
        },

        // Blendet Alt Date ein oder aus
        toggleAltDate: function(e) {
            e.preventDefault();
            var altDate = this.$el.find('.date.alt');
            altDate.find('>').toggleClass('bn__hidden');
            altDate.find('input').val('');
        },

        initValidation: function() {
            var that = this;

            // Wenn parsley aktiviert und clientseitige Validierung gesetzt
            if($.fn.parsley && this.options.validation) {

                this.$el.find('.bn--request').parsley({
                    focus: 'none',
                    trigger: 'focusout',
                    errorClass: 'notvalid',
                    // eHotelier Fehler nachahmen
                    errors: {
                        container: function() {
                            return '';
                        },
                        classHandler: function(el) {
                            return el;
                        }
                    },
                    listeners: {
                        // Wenn Fehler springe zu Fehler
                        onFormSubmit: function(isFormValid) {
                            if(!isFormValid) {
                                that.scrollToFirstError();
                            }
                        }
                    }
                });
            }
        },

        // Initialisiert jQuery UI Datepicker wenn verfügbar
        initUiDatepicker: function(fromInput, toInput) {
            if($.ui && $.datepicker) {

                var that = this,
                    fromDate = this.$el.find(fromInput),
                    toDate = this.$el.find(toInput);

                fromDate.datepicker({
                    defaultDate: '+1d',
                    minDate: '+1d',
                    dateFormat: 'dd.mm.yy',
                    beforeShow: function() {
                        that.resizeUiDatepicker($(this));
                    },
                    onChangeMonthYear: function() {
                        that.resizeUiDatepicker($(this));
                    },
                    onSelect: function() {
                        // Setze minDate von toDate + 1 Tag
                        var date = $(this).datepicker('getDate');
                        date.setDate(date.getDate() + 1);

                        toDate
                            .datepicker('option', 'minDate', date)
                            .datepicker('setDate','');
                    },
                    onClose: function(dateText) {
                        // Wurde Datum gewählt, zeige nächsten Datepicker
                        if(dateText) {
                            var d = fromDate.datepicker('getDate');
                            d.setDate(d.getDate()+7);

                            setTimeout(function() {
                                toDate.datepicker('setDate', d);
                                toDate.datepicker('show');
                            }, 0);
                        }
                        if($.fn.parsley && that.options.validation) {
                            fromDate.parsley('validate');
                        }
                    }
                });

                toDate.datepicker({
                    defaultDate: '+7d',
                    minDate: '+2d',
                    dateFormat: 'dd.mm.yy',
                    beforeShow: function() {
                        that.resizeUiDatepicker($(this));
                    },
                    onChangeMonthYear: function() {
                        that.resizeUiDatepicker($(this));
                    },
                    onClose: function() {
                        if($.fn.parsley && that.options.validation) {
                            toDate.parsley('validate');
                        }
                    }
                });

                // Icons klickbar
                this.$el.find('.date').on('click', '.calendar', function() {
                    $(this).siblings('.hasDatepicker').datepicker('show');
                });

            }
        },

        // Responsive jQuery UI Datepicker
        resizeUiDatepicker: function ($input) {
            var width = $input.outerWidth();
            setTimeout(function() {
                $input.datepicker('widget').css('width', width);
            }, 0);
        },

        // Blendet Facebook Button ein
        showFb: function() {
            if(this.options.facebook) {
                this.$el.find('.facebook--login').removeClass('bn__hidden');
                this.$el.on('click', '.facebook--login', $.proxy(this.getFbData, this));
            }
        },

        // Lädt Facebook Formular
        getFbData: function(e) {
            e.preventDefault();
            var button = $(e.currentTarget);
            if(!button.hasClass('bn__disabled')) {
                this.$el.append('<iframe id="'+this.id+'_fbdataiframe" name="fbdataiframe" width="0" height="0" border="0" frameborder="0" src="http://admin.ehotelier.it/fbform/fbform.php?domain='+document.domain+'&formcontainer_id='+this.id+'"></iframe>');
                button.addClass('bn__disabled');

                var that = this;

                // Facebook
                window.setfbdata = function(data) {
                    //TODO remove if tests pass
                    console.log(data);

                    // Decode Data
                    for(var prop in data) {
                        if(data.hasOwnProperty(prop)) {
                            data[prop] = decodeURIComponent(data[prop]);
                        }
                    }

                    that.setField(data);
                };
            }
        },

        // Fügt Zimmer hinzu
        appendRoom: function(e) {
            if(e) {
                e.preventDefault();
            }

            this.cache.room = this.cache.room || {};

            // Cache
            if (!this.cache.room.template) {
                // Hole Template
                this.cache.room.id = 0;
                this.cache.room.template = $.trim(this.$el.find('.roomTpl').html());
                this.cache.room.container = this.$el.find('.bn--request__rooms');
            } else {
                this.cache.room.id++;
            }

            var template = this.cache.room.template.replace(/\$\$name\$\$/g, this.cache.room.id);

            // Events initialisieren
            template = $(template)
                            .on('click','.removeRoom', $.proxy(this.removeRoom, this))
                            .on('change','#'+this.id+'_otatransferrooms_'+this.cache.room.id+'_asacode', $.proxy(this.changeRoomType, this));

            // IE 8 verschiedenfarbige Zimmer
            if(this.cache.room.id % 2) {
                template.addClass('odd');
            }

            // Zimmerfilter anwenden
            if(this.options.room_filter) {
                var select = template.find('#'+this.id+'_otatransferrooms_'+this.cache.room.id+'_asacode');

                select.find('option:gt(0)').remove();
                $.each(this.hotelData, function(key, value) {
                    select.append('<option value="'+value.asacode+'">'+value.name+'</option>');
                });
            }

            // Template DOM hinzufügen
            this.cache.room.container.find('.addRoom').before(template);

            // Setze Parsley Validierung
            if($.fn.parsley && this.options.validation) {
                this.$el.find('.bn--request').parsley('addItem', '#'+this.id+'_otatransferrooms_'+this.cache.room.id+'_asacode');
            }
        },

        // Entfernt Zimmer
        removeRoom: function(e) {
            e.preventDefault();
            var room = $(e.currentTarget).closest('.bn--request__room');

            // Entferne Parsley Validierung
            if($.fn.parsley && this.options.validation) {
                room.find('.parsley-validated').parsley('removeConstraint', 'required');
            }
             // Entferne Eventlistener und lösche Element
            room.remove();

        },

        // Passt Inputs der Zimmer an ausgewählten Zimmertyp an
        changeRoomType: function(e) {
            var room = $(e.currentTarget).closest('.bn--request__room'),
                roomId = room.data('id'),
                asaCode = $(e.currentTarget).val(),
                details = room.find('.bn--request__room--details');

            if(asaCode) {
                details.removeClass('bn__hidden');
            } else {
                details.addClass('bn__hidden');
            }

            // Holt Zimmerdaten
            if(asaCode) {
                var roomData = this.getRoomDataByASACode(asaCode),
                    boardSelect = room.find('#'+this.id+'_otatransferrooms_'+roomId+'_board'),
                    guestSelect = room.find('#'+this.id+'_otatransferrooms_'+roomId+'_guestcounts'),
                    childSelect = room.find('.childrencounts');


                // Limitiert Select Boxen
                this.limitBoards(boardSelect, roomData.roomtypeboards);
                this.limitGuests(guestSelect, childSelect, roomData, true);

                // Setzt Events
                guestSelect.off().on('change', $.proxy(this.limitGuests, this, guestSelect, childSelect, roomData, false));
                childSelect.off().on('change', $.proxy(this.limitGuests, this, guestSelect, childSelect, roomData, false));
            }
        },

        // Holt Daten eines Zimmers anhand des ASA Codes
        getRoomDataByASACode: function(asacode) {
            for(var i = 0, len = this.hotelData.length; i < len; i++) {
                if(this.hotelData[i].asacode === asacode) {
                    return this.hotelData[i];
                }
            }
        },

        // Limitiert select boxen
        limitBoards: function(select, roomData) {
            this.cache.board = this.cache.board || {};

            // Cacht orig Selectbox
            if(!this.cache.board.options) {
                this.cache.board.options = select.find('option').clone();
            } else {
                select.empty().append(this.cache.board.options.clone());
            }

            select.find('option').filter(function(index, option) {
                var toDelete = true;

                // Loop durch RoomData
                for(var i = 0, len = roomData.length; i < len; i++) {
                    // Wenn Zimmer gefunden
                    if(roomData[i].board_id === parseInt(option.value, 10)) {
                        toDelete = false;

                        // Default Board auswählen
                        if(roomData[i].isdefault) {
                            $(this).attr('selected', true);
                        }
                    }
                }
                return toDelete;
            }).remove();
        },

        // Limitiert Anzahl von Erwachsene
        limitAdults: function(select, val, max) {
            this.cache.guests = this.cache.guests || {};

            // Cacht orig Selectbox
            if(!this.cache.guests.options) {
                this.cache.guests.options = select.find('option').clone();
            } else {
                select.empty().append(this.cache.guests.options.clone());
            }

            select.find('option:gt('+(max)+')').remove();

            select.val(val);
        },

        // Limitiert Anzahl von Kinder
        limitChilds: function(select, val, max) {

            this.cache.child = this.cache.child || {};

            // Cacht orig Selectbox
            if(!this.cache.child.options) {
                this.cache.child.options = select.find('option').clone();
            } else {
                select.empty().append(this.cache.child.options.clone());
            }

            select.find('option:gt('+(max)+')').remove();

            select.val(val);
        },

        // Fügt Kinder Alterauswahl hinzu
        appendChildAge: function(select) {

            this.cache.child = this.cache.child || {};

            // Cache
            if (!this.cache.child.template) {
                // Hole Template
                this.cache.child.count = 1;
                this.cache.child.template = $.trim(this.$el.find('.childageTpl').html());
            }

            var selectedChilds = select.val(), // Lese anzuzeigende Zimmeranzahl aus
                room =  select.closest('.bn--request__room'),
                templates = '',
                childcontainer = room.find('.childages');

            // Wenn Selected gleich Anzahl Kinder tue nichts
            if(selectedChilds === this.cache.child.count) {
                return;
            }

            for(var i = 1; i <= selectedChilds; i++) {
                templates += this.cache.child.template;
            }

            // Füge alle Templates in einmal hinzu
            childcontainer.find('.bn__col').empty().append(templates);

            if(parseInt(selectedChilds, 10) !== 0) {
                childcontainer.removeClass('bn__hidden');
            } else {
                childcontainer.addClass('bn__hidden');
            }
        },


        // Je nach Gästeanzahl werden option Felder ausgeblendet thx @ Benni & Marci
        limitGuests: function(adultsSelect, childSelect, roomData, isRoomChanged) {

            var adultsVal = roomData.defaultpersons,
                childVal = 0;

            if(!isRoomChanged) {
                adultsVal = parseInt(adultsSelect.val(), 10);
                childVal = parseInt(childSelect.val(), 10);
            }

            var guestsLeft = roomData.maxpersons - (adultsVal + childVal);

            adultsSelect.val(adultsVal);
            childSelect.val(childSelect);

            this.limitAdults(adultsSelect, adultsVal, guestsLeft + adultsVal - 1);
            this.limitChilds(childSelect, childVal, guestsLeft + childVal);

            this.appendChildAge(childSelect);
        },

        // Schickt Formular ab
        submitForm: function(e) {
            e.preventDefault();
            var submitButton = $(e.currentTarget);

            // Verhindert das mehrmalige Abschicken vom Formular
            this.cache.formSubmitted = this.cache.formSubmitted || false;

            // Setzt Hidden Felder
            this.setField({
                'generatorurl': document.location.href,
                'doubleopturl': this.options.url_doubleopt
            });
            this.setChilds();

            // Eventhandler für Entwickler
            if (typeof this.options.onSubmit !== 'undefined') {
                this.options.onSubmit(this);
            }

            // Validiere Formular
            if($.fn.parsley && this.options.validation) {
                if(this.$el.find('.bn--request').parsley('validate')) {
                    this.postForm(submitButton);
                }
            } else {
                if(!this.cache.formSubmitted) {
                    this.cache.formSubmitted = true;
                    this.postForm(submitButton);
                }
            }
        },

        setServerErrors: function(errors) {
            // Loop durch Fehler
            var that = this;

            // Entfernt vorherige Fehler
            this.$el.find('.notvalid').removeClass('notvalid');
            this.cache.formSubmitted = false;

            // TODO Rekursive Funktion
            $.each(errors, function(key, value) {
                that.$el.find('#'+that.id+'_'+key).addClass('notvalid');

                if(key === 'otatransferrooms') {
                    $.each(value, function(key2, value2) {
                        $.each(value2, function(key3) {
                            that.$el.find('#'+that.id+'_'+key+'_'+key2+'_'+key3).addClass('notvalid');
                        });
                    });
                }
            });

            this.scrollToFirstError();
        },

        scrollToFirstError: function() {
            var firstError = this.$el.find('.notvalid:first').prev('label');

            $('html, body').animate({
                scrollTop: firstError.offset().top
            }, 700);
        },

        // Sendet Daten an eHotelier
        postForm: function(submitButton) {
            submitButton.addClass('submitted');

            $.ajax({
                url: 'bnproxy.php?path=makereservation&type=request&formcontainer_id='+this.options.formcontainer_id+'&test='+this.options.test+'&callback=?',
                dataType: 'json',
                context: this,
                data: this.$el.find('.bn--request').serialize(),
                success: function(data) {
                    if(data.errors) {
                        // Eventhandler für Entwickler
                        if(typeof this.options.onError !== 'undefined') {
                            this.options.onError(this);
                        }
                        submitButton.removeClass('submitted');
                        // Wenn Fehler, diese setzen
                        this.setServerErrors(data.errors);
                    } else {
                        // Eventhandler für Entwickler
                        if(typeof this.options.onSuccess !== 'undefined') {
                            this.options.onSuccess(this);
                        }
                        // Ansonsten weiterleiten
                        window.location.href=this.options.url_success;
                    }
                }
            });
        },

        // Setzt beliebig viele Feld/er
        setField: function(key, value) {
            var attrs = {};

            // Generiert aus Parameter Objekt
            if (typeof key === 'object') {
                attrs = key;
            } else {
                attrs[key] = value;
            }

            for(var prop in attrs) {
                if(attrs.hasOwnProperty(prop)) {
                    this.$el.find('#'+this.id+'_'+prop).val(attrs[prop]).change();
                }
            }
        },

        // Holt Feld der Wahl
        getField: function(key) {
            return this.$el.find('#'+this.id+'_'+key).val();
        },

        // Ausgewählte Kinder in hidden input schreiben
        setChilds: function() {
            var that = this;
            this.$el.find('.bn--request__room').each(function() {
                var $this = $(this),
                    roomId = $this.data('id'),
                    childs = [];

                $this.find('.childages select').each(function(){
                    childs.push($(this).val());
                });

                $this.find('#'+that.id+'_otatransferrooms_'+roomId+'_childrenages').val(childs.join(','));
            });
        }
    };


})(jQuery, window, document);
