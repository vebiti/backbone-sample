/*global Backbone, _*/

(function () {
    'use strict';

    var app = require('../app.js'),
        Backbone = require('backbone'),
        _ = require('lodash');

    module.exports = Backbone.Collection.extend({
        //model: app.Models.Accomodation,
        url: 'bnproxy.php?path=services/hoteldata/de&' +
            'portal_id=0&hotel_ids=1,2,4,6,9,10,11,12,878,1053&fields={"*":null}',
        parse: function (response) {
            return response.records;
        },

        customFilter: function (activeFilters) {

            // if at least one filter is active, return the filtered collection,
            // otherwise the entire collection

            if (!activeFilters) {
                activeFilters = [];
            }

            if (activeFilters.length) {
                var results = this.filter(function (currentModel) {
                    var modelType = currentModel.get('hotelcategory_id'),
                        match = false;

                    _(activeFilters).forEach(function (filteredType) {
                        if (filteredType === modelType) {
                            match = true;
                        }
                    });

                    return match;
                });

                return new app.Collections.Accomodations(results);

            } else {
                return this;
            }
        }
    });



}());
