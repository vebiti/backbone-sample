(function () {
    'use strict';

    var gulp = require('gulp'),
        browserify = require('browserify'),
        livereload = require('gulp-livereload'),
        rename = require('gulp-rename'),
        source = require('vinyl-source-stream'),
        streamify = require('gulp-streamify'),
        stripDebug = require('gulp-strip-debug'),
        uglify = require('gulp-uglify');


    gulp.task('browserify', function () {
            browserify({
                entries: ['./js/main.js']
            })
            //.require('lodash', { expose: 'underscore' })
            .bundle()
            .pipe(source('main.js'))
            .pipe(rename('bundle.js'))
            .pipe(streamify(uglify()))
            .pipe(streamify(stripDebug()))
            .pipe(gulp.dest('assets/js'));
            //.pipe(livereload());
    });

    gulp.watch('./js/**', ['browserify']);

    gulp.task('default', ['browserify']);

}());
